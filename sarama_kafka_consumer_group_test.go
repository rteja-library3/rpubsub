package rpubsub_test

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/rteja-library3/rpubsub"
	"gitlab.com/rteja-library3/rpubsub/mocks"

	"github.com/Shopify/sarama"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/mock"
)

func TestNewSaramaKafkaConsumerGroupAdapterWithPropertySuccess(t *testing.T) {
	var fn rpubsub.CreateSaramaConsumerGroup = func(addrs []string, groupID string, config *sarama.Config) (sarama.ConsumerGroup, error) {
		return new(mocks.SaramaConsumerGroup), nil
	}

	prop := rpubsub.SaramaKafkaConsumserGroupAdapterProperty{
		[]string{"kafka1.com", "kafka2.com", "kafka3.com"},
		"test-group",
		[]string{"test-topic"},
		rpubsub.NewDefaultSaramaConsumerGroupHandler("service-test", &mocks.EventHandler{}, &mocks.DLQHandler{}),
		sarama.NewConfig(),
		fn,
	}

	subscriber, err := rpubsub.NewSaramaKafkaConsumerGroupAdapterWithProperty(
		logrus.New(),
		prop,
	)

	assert.NoError(t, err, "[TestNewSaramaKafkaConsumerGroupAdapterWithPropertySuccess] Should not error")
	assert.NotNil(t, subscriber, "[TestNewSaramaKafkaConsumerGroupAdapterWithPropertySuccess] Should not nil")
}

func TestNewSaramaKafkaConsumerGroupAdapterWithPropertyError(t *testing.T) {
	prop := rpubsub.SaramaKafkaConsumserGroupAdapterProperty{
		[]string{"kafka1.com", "kafka2.com", "kafka3.com"},
		"test-group",
		[]string{"test-topic"},
		rpubsub.NewDefaultSaramaConsumerGroupHandler("service-test", &mocks.EventHandler{}, &mocks.DLQHandler{}),
		sarama.NewConfig(),
		nil,
	}

	subscriber, err := rpubsub.NewSaramaKafkaConsumerGroupAdapterWithProperty(
		logrus.New(),
		prop,
	)

	assert.Error(t, err, "[TestNewSaramaKafkaConsumerGroupAdapterWithPropertyError] Should error")
	assert.Nil(t, subscriber, "[TestNewSaramaKafkaConsumerGroupAdapterWithPropertyError] Should nil")
}

func TestSaramaKafkaConsumserGroupAdapter_Success(t *testing.T) {
	cgHandler := rpubsub.NewDefaultSaramaConsumerGroupHandler("service-test", &mocks.EventHandler{}, &mocks.DLQHandler{})
	topics := []string{"test-topic"}

	cg := new(mocks.SaramaConsumerGroup)
	cg.On("Consume", mock.Anything, mock.AnythingOfType("[]string"), mock.AnythingOfType("*rpubsub.DefaultSaramaConsumerGroupHandler")).
		Return(nil)

	cg.On("Close").
		Return(nil)

	subscriber := rpubsub.NewSaramaKafkaConsumserGroupAdapter(logrus.New(), &rpubsub.SaramaKafkaConsumserGroupAdapterConfig{
		ConsumerGroupClient:  cg,
		ConsumerGroupHandler: cgHandler,
		Topics:               topics,
	})

	subscriber.Subscribe()
	<-time.After(time.Millisecond * 10)
	subscriber.Close()

	cg.AssertExpectations(t)
}

func TestSaramaKafkaConsumserGroupAdapter_ConsumeError(t *testing.T) {
	cgHandler := rpubsub.NewDefaultSaramaConsumerGroupHandler("service-test", &mocks.EventHandler{}, &mocks.DLQHandler{})
	topics := []string{"test-topic"}

	cg := new(mocks.SaramaConsumerGroup)
	cg.On("Consume", mock.Anything, mock.AnythingOfType("[]string"), mock.AnythingOfType("*rpubsub.DefaultSaramaConsumerGroupHandler")).
		Return(sarama.ErrOutOfBrokers)

	cg.On("Close").
		Return(nil)

	subscriber := rpubsub.NewSaramaKafkaConsumserGroupAdapter(logrus.New(), &rpubsub.SaramaKafkaConsumserGroupAdapterConfig{
		ConsumerGroupClient:  cg,
		ConsumerGroupHandler: cgHandler,
		Topics:               topics,
	})

	subscriber.Subscribe()
	<-time.After(time.Millisecond * 10)
	subscriber.Close()

	cg.AssertExpectations(t)
}

func TestSaramaKafkaConsumserGroupAdapter_ClosingError(t *testing.T) {
	cgHandler := rpubsub.NewDefaultSaramaConsumerGroupHandler("service-test", &mocks.EventHandler{}, &mocks.DLQHandler{})
	topics := []string{"test-topic"}

	cg := new(mocks.SaramaConsumerGroup)
	cg.On("Consume", mock.Anything, mock.AnythingOfType("[]string"), mock.AnythingOfType("*rpubsub.DefaultSaramaConsumerGroupHandler")).
		Return(nil)

	cg.On("Close").
		Return(sarama.ErrBrokerNotAvailable)

	subscriber := rpubsub.NewSaramaKafkaConsumserGroupAdapter(logrus.New(), &rpubsub.SaramaKafkaConsumserGroupAdapterConfig{
		ConsumerGroupClient:  cg,
		ConsumerGroupHandler: cgHandler,
		Topics:               topics,
	})

	subscriber.Subscribe()
	<-time.After(time.Millisecond * 10)
	subscriber.Close()

	cg.AssertExpectations(t)
}
