package rpubsub

import (
	"context"

	"github.com/Shopify/sarama"
)

// SaramaConsumerGroup is an interface that purposed for mock creation for unit testing.
// Do not use this for an implementation.
type SaramaConsumerGroup interface {
	Consume(ctx context.Context, topics []string, handler sarama.ConsumerGroupHandler) error
	Errors() <-chan error
	Close() error
	Pause(partitions map[string][]int32)
	Resume(partitions map[string][]int32)
	PauseAll()
	ResumeAll()
}

type SaramaAsyncProducer interface {
	AsyncClose()
	Close() error
	Input() chan<- *sarama.ProducerMessage
	Successes() <-chan *sarama.ProducerMessage
	Errors() <-chan *sarama.ProducerError
}

// SaramaConsumerGroupSession is an interface that purposed for mock creation for unit testing.
// Do not use this for an implementation.
type SaramaConsumerGroupSession interface {
	Claims() map[string][]int32
	MemberID() string
	GenerationID() int32
	MarkOffset(topic string, partition int32, offset int64, metadata string)
	Commit()
	ResetOffset(topic string, partition int32, offset int64, metadata string)
	MarkMessage(msg *sarama.ConsumerMessage, metadata string)
	Context() context.Context
}

// SaramaConsumerGroupClaim is an interface that purposed for mock creation for unit testing.
// Do not use this for an implementation.
type SaramaConsumerGroupClaim interface {
	Topic() string
	Partition() int32
	InitialOffset() int64
	HighWaterMarkOffset() int64
	Messages() <-chan *sarama.ConsumerMessage
}
