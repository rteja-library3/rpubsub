package rpubsub_test

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/rteja-library3/rpubsub"
	"gitlab.com/rteja-library3/rpubsub/mocks"
)

func TestDQLHandlerSend(t *testing.T) {
	t.Run("Success", func(t *testing.T) {
		publisher := new(mocks.Publisher)
		publisher.On("Send", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).
			Return(nil)

		msg := &rpubsub.DeadLetterQueueMessage{}

		dlq := rpubsub.NewDLQHandlerAdapter("test", publisher)

		err := dlq.Send(context.Background(), msg)

		assert.NoError(t, err, "[TestDQLHandlerSend] Should not err")

		publisher.AssertExpectations(t)
	})

	t.Run("Message nil", func(t *testing.T) {
		publisher := new(mocks.Publisher)

		dlq := rpubsub.NewDLQHandlerAdapter("test", publisher)

		err := dlq.Send(context.Background(), nil)

		assert.NoError(t, err, "[TestDQLHandlerSend] Should not err")

		publisher.AssertExpectations(t)
	})
}
