package rpubsub

import "github.com/Shopify/sarama"

type CreateSaramaConsumerGroup func(addrs []string, groupID string, config *sarama.Config) (sarama.ConsumerGroup, error)

var (
	DefaultCreateSaramaConsumerGroup CreateSaramaConsumerGroup = sarama.NewConsumerGroup
)
