package rpubsub

import "time"

type OptionsPublisher struct {
	offset    int64
	partition int32
	timestamp time.Time
	metadata  interface{}
	printLog  bool
}

func (o *OptionsPublisher) SetOffset(offset int64) *OptionsPublisher {
	o.offset = offset
	return o
}

func (o *OptionsPublisher) SetPartition(partition int32) *OptionsPublisher {
	o.partition = partition
	return o
}

func (o *OptionsPublisher) SetTimestamp(timestamp time.Time) *OptionsPublisher {
	o.timestamp = timestamp
	return o
}

func (o *OptionsPublisher) SetMetadata(metadata interface{}) *OptionsPublisher {
	o.metadata = metadata
	return o
}

func (o *OptionsPublisher) SetPrintLog(printLog bool) *OptionsPublisher {
	o.printLog = printLog
	return o
}

func NewOptionsPublisher() *OptionsPublisher {
	return &OptionsPublisher{}
}

func MergeOptionsPublisher(options ...*OptionsPublisher) *OptionsPublisher {
	opt := NewOptionsPublisher()

	for _, option := range options {
		if option.offset > 0 {
			opt.offset = option.offset
		}

		if option.partition > 0 {
			opt.partition = option.partition
		}

		if !option.timestamp.IsZero() {
			opt.timestamp = option.timestamp
		}

		if option.metadata != nil {
			opt.metadata = option.metadata
		}

		opt.printLog = option.printLog
	}

	return opt
}
