package rpubsub

type MessageHeaders map[string][]byte

func (mh MessageHeaders) Add(key string, value []byte) {
	mh[key] = value
}

func (mh MessageHeaders) Get(key string) (value []byte) {
	value = mh[key]
	return
}

func (mh MessageHeaders) String(key string) (value string) {
	value = string(mh.Get(key))
	return
}
