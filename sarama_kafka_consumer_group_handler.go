package rpubsub

import (
	"context"
	"log"
	"time"

	"github.com/Shopify/sarama"
	"gitlab.com/rteja-library3/rhelper"
)

type DefaultSaramaConsumerGroupHandler struct {
	timeLoc      *time.Location
	consumer     string
	eventHandler EventHandler
	dlqHandler   DLQHandler
}

func NewDefaultSaramaConsumerGroupHandler(consumer string, eventHandler EventHandler, dlqHandler DLQHandler) *DefaultSaramaConsumerGroupHandler {
	return &DefaultSaramaConsumerGroupHandler{
		timeLoc:      rhelper.TimeLocationUTC,
		consumer:     consumer,
		eventHandler: eventHandler,
		dlqHandler:   dlqHandler,
	}
}

func (consumer *DefaultSaramaConsumerGroupHandler) Setup(sarama.ConsumerGroupSession) error {
	// Mark the consumer as ready
	// close(consumer.ready)
	return nil
}

func (consumer *DefaultSaramaConsumerGroupHandler) Cleanup(sarama.ConsumerGroupSession) error {
	return nil
}

func (consumer *DefaultSaramaConsumerGroupHandler) ConsumeClaim(session sarama.ConsumerGroupSession, claim sarama.ConsumerGroupClaim) error {
	// NOTE:
	// Do not move the code below to a goroutine.
	// The `ConsumeClaim` itself is called within a goroutine, see:
	// https://github.com/Shopify/sarama/blob/master/consumer_group.go#L27-L29
	for message := range claim.Messages() {
		consumer.claim(session.Context(), message)
		session.MarkMessage(message, "")
	}

	return nil
}

func (consumer *DefaultSaramaConsumerGroupHandler) claim(ctx context.Context, message *sarama.ConsumerMessage) {
	if consumer.eventHandler == nil {
		consumer.printMessage(message)
		return
	}

	err := consumer.eventHandler.Handle(ctx, message)
	if err != nil {
		consumer.sendToDLQ(ctx, message, err)
	}
}

func (consumer *DefaultSaramaConsumerGroupHandler) printMessage(message *sarama.ConsumerMessage) {
	log.Printf("Message claimed: value = %s, timestamp = %v, topic = %s, partition = %d", string(message.Value), message.Timestamp, message.Topic, message.Partition)
}

func (consumer *DefaultSaramaConsumerGroupHandler) sendToDLQ(ctx context.Context, message *sarama.ConsumerMessage, err error) {
	if consumer.dlqHandler == nil {
		return
	}

	originalHeaders := MessageHeaders{}
	for _, header := range message.Headers {
		originalHeaders.Add(string(header.Key), header.Value)
	}

	dlqMessage := &DeadLetterQueueMessage{
		Channel:   message.Topic,
		Publisher: originalHeaders.String("origin"),
		Consumer:  consumer.consumer,
		Key:       string(message.Key),
		Headers:   originalHeaders,
		Message:   string(message.Value),
		Reason:    err.Error(),
		FailedAt:  message.Timestamp.In(consumer.timeLoc),
	}

	consumer.dlqHandler.Send(ctx, dlqMessage)
}
