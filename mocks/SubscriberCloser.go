// Code generated by mockery v1.0.0. DO NOT EDIT.

package mocks

import mock "github.com/stretchr/testify/mock"

// SubscriberCloser is an autogenerated mock type for the SubscriberCloser type
type SubscriberCloser struct {
	mock.Mock
}

// Close provides a mock function with given fields:
func (_m *SubscriberCloser) Close() error {
	ret := _m.Called()

	var r0 error
	if rf, ok := ret.Get(0).(func() error); ok {
		r0 = rf()
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// Subscribe provides a mock function with given fields:
func (_m *SubscriberCloser) Subscribe() {
	_m.Called()
}
