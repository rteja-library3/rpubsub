package rpubsub_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/rteja-library3/rpubsub"
)

func TestMessageHeaders(t *testing.T) {
	t.Run("Add", func(t *testing.T) {
		header := rpubsub.MessageHeaders{}
		header.Add("tes", []byte("a"))

		assert.NotNil(t, header, "[TestMessageHeaders] Should not nil")
	})

	t.Run("Get nil", func(t *testing.T) {
		header := rpubsub.MessageHeaders{}
		result := header.Get("tes")

		assert.Nil(t, result, "[TestMessageHeaders] Should result nil")
	})

	t.Run("Get value", func(t *testing.T) {
		header := rpubsub.MessageHeaders{
			"tes": []byte("a"),
		}

		result := header.Get("tes")

		assert.NotNil(t, result, "[TestMessageHeaders] Result should not nil")
		assert.Equal(t, []byte("a"), result, "[TestMessageHeaders] Result should []byte(\"a\")")
	})

	t.Run("Get as string", func(t *testing.T) {
		header := rpubsub.MessageHeaders{
			"tes": []byte("a"),
		}

		result := header.String("tes")

		assert.NotNil(t, result, "[TestMessageHeaders] Result should not nil")
		assert.Equal(t, "a", result, "[TestMessageHeaders] Result should \"a\"")
	})
}
