package rpubsub_test

import (
	"context"
	"testing"

	"github.com/Shopify/sarama"
	"github.com/Shopify/sarama/mocks"
	"github.com/sirupsen/logrus"
	"gitlab.com/rteja-library3/rpubsub"
)

func TestSaramaKafkaProducer(t *testing.T) {
	t.Run("when the message is successfully sent and delivered", func(t *testing.T) {
		saramaProducer := mocks.NewAsyncProducer(t, nil)
		saramaProducer.ExpectInputAndSucceed()

		publisher := rpubsub.NewSaramaKafkaProducerAdapter(logrus.New(), &rpubsub.SaramaKafkaProducerAdapterConfig{
			AsyncProducer: saramaProducer,
		})

		headers := rpubsub.MessageHeaders{}
		headers.Add("test", []byte("header"))

		opts := rpubsub.NewOptionsPublisher().SetPrintLog(true)

		publisher.Send(context.TODO(), "test-topic", "test-key", headers, []byte("Hola"), opts)
		publisher.Close()
	})

	t.Run("when the connection is timeout", func(t *testing.T) {
		saramaProducer := mocks.NewAsyncProducer(t, nil)
		saramaProducer.ExpectInputAndFail(sarama.ErrRequestTimedOut)

		publisher := rpubsub.NewSaramaKafkaProducerAdapter(logrus.New(), &rpubsub.SaramaKafkaProducerAdapterConfig{
			AsyncProducer: saramaProducer,
		})

		headers := rpubsub.MessageHeaders{}
		headers.Add("test", []byte("header"))

		publisher.Send(context.TODO(), "test-topic", "test-key", headers, []byte("Hola"))
		publisher.Close()
	})

	t.Run("when the message channel is already closed", func(t *testing.T) {
		saramaProducer := mocks.NewAsyncProducer(t, nil)

		publisher := rpubsub.NewSaramaKafkaProducerAdapter(logrus.New(), &rpubsub.SaramaKafkaProducerAdapterConfig{
			AsyncProducer: saramaProducer,
		})

		headers := rpubsub.MessageHeaders{}
		headers.Add("test", []byte("header"))

		publisher.Close()
		publisher.Send(context.TODO(), "test-topic", "test-key", headers, []byte("Hola"))
	})
}
