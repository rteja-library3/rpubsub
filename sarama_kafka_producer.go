package rpubsub

import (
	"context"

	"github.com/Shopify/sarama"
	"github.com/sirupsen/logrus"
)

// SaramaKafkaProducerAdapterConfig is a configuration of sarama kafka adapter.
type SaramaKafkaProducerAdapterConfig struct {
	AsyncProducer sarama.AsyncProducer
}

// SaramaKafkaProducerAdapter is a concrete struct of sarma kafka adapter.
type SaramaKafkaProducerAdapter struct {
	logger *logrus.Logger
	config *SaramaKafkaProducerAdapterConfig
}

// NewSaramaKafkaProducerAdapter is a constructor.
func NewSaramaKafkaProducerAdapter(logger *logrus.Logger, config *SaramaKafkaProducerAdapterConfig) PublisherCloser {
	p := &SaramaKafkaProducerAdapter{
		logger,
		config,
	}
	go p.run()

	return p
}

func (skpa *SaramaKafkaProducerAdapter) run() {
	for producerError := range skpa.config.AsyncProducer.Errors() {
		skpa.logger.Errorf("[Sarama] %s", producerError.Error())
	}
}

func (skpa *SaramaKafkaProducerAdapter) buildRecordHeaders(headers MessageHeaders) []sarama.RecordHeader {
	line := 0
	recordHeaders := make([]sarama.RecordHeader, len(headers))

	for key, value := range headers {
		recordHeader := sarama.RecordHeader{
			Key:   []byte(key),
			Value: value,
		}

		recordHeaders[line] = recordHeader
		line++
	}

	return recordHeaders
}

func (skpa *SaramaKafkaProducerAdapter) Send(ctx context.Context, topic string, key string, headers MessageHeaders, message []byte, options ...*OptionsPublisher) (err error) {
	defer func() {
		r := recover()
		if r != nil {
			skpa.logger.
				Errorf("[Sarama] %v", r)
		}
	}()

	opt := MergeOptionsPublisher(options...)

	if opt.printLog {
		// printing log
		skpa.logger.
			Info("[Sarama] Producer sending message topic ", topic, " message ", string(message))
	}

	channel := skpa.config.AsyncProducer.Input()
	channel <- &sarama.ProducerMessage{
		Topic:     topic,
		Headers:   skpa.buildRecordHeaders(headers),
		Key:       sarama.ByteEncoder(key),
		Value:     sarama.ByteEncoder(message),
		Offset:    opt.offset,
		Partition: opt.partition,
		Timestamp: opt.timestamp,
		Metadata:  opt.metadata,
	}

	return
}

// Close will stop the producer
func (skpa *SaramaKafkaProducerAdapter) Close() (err error) {
	err = skpa.config.AsyncProducer.Close()
	skpa.logger.Info("[Sarama] Producer is gracefully shutdown")
	return
}
