package rpubsub_test

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/rteja-library3/rpubsub"
)

func TestMergeOptionsPublisher(t *testing.T) {
	t.Run("Success", func(t *testing.T) {
		opts := rpubsub.NewOptionsPublisher().
			SetMetadata("a").
			SetOffset(1).
			SetPartition(1).
			SetPrintLog(false).
			SetTimestamp(time.Now())

		opt := rpubsub.MergeOptionsPublisher(opts)

		assert.NotNil(t, opt, "[TestMergeOptionsPublisher] Should not nil")
	})
}
