package rpubsub

import (
	"context"
	"encoding/json"
	"fmt"
	"time"
)

type DeadLetterQueueMessage struct {
	Channel   string         `json:"channel"`
	Publisher string         `json:"publisher"`
	Consumer  string         `json:"consumer"`
	Key       string         `json:"key"`
	Headers   MessageHeaders `json:"headers"`
	Message   string         `json:"message"`
	Reason    string         `json:"reason"`
	FailedAt  time.Time      `json:"failedAt"`
}

type DLQHandlerAdapter struct {
	topic     string
	publisher Publisher
}

func NewDLQHandlerAdapter(topic string, publisher Publisher) *DLQHandlerAdapter {
	return &DLQHandlerAdapter{
		topic,
		publisher,
	}
}

func (dlqHandlerAdapter *DLQHandlerAdapter) Send(ctx context.Context, dlqMessage *DeadLetterQueueMessage) (err error) {
	if dlqMessage == nil {
		return
	}

	if dlqMessage.Headers == nil {
		dlqMessage.Headers = MessageHeaders{}
	}

	dlqMessage.Headers.Add("dlq", []byte("true"))

	key := fmt.Sprintf("%s:%s:%s:%d",
		dlqMessage.Consumer,
		dlqMessage.Channel,
		dlqMessage.Key,
		time.Now().UnixNano(),
	)

	marshaledMessage, _ := json.Marshal(dlqMessage)
	topics := dlqHandlerAdapter.topic

	err = dlqHandlerAdapter.publisher.Send(
		ctx,
		topics,
		key,
		dlqMessage.Headers,
		marshaledMessage,
	)
	return
}
