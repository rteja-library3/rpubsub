package rpubsub

import "context"

type DLQHandler interface {
	Send(ctx context.Context, dlqMessage *DeadLetterQueueMessage) (err error)
}

type EventHandler interface {
	Handle(ctx context.Context, message interface{}) (err error)
}

type Closer interface {
	Close() (err error)
}

type Publisher interface {
	Send(ctx context.Context, topic string, key string, headers MessageHeaders, message []byte, options ...*OptionsPublisher) (err error)
}

type PublisherCloser interface {
	Publisher
	Closer
}

type Subscriber interface {
	Subscribe()
}

type SubscriberCloser interface {
	Subscriber
	Closer
}
