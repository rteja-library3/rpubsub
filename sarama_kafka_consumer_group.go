package rpubsub

import (
	"context"

	"github.com/Shopify/sarama"
	"github.com/sirupsen/logrus"
)

// SaramaKafkaConsumserGroupAdapterConfig is a configuration.
//
// FIELDS:
//
// `ConsumerGroupClient` is client that returned from `sarama.NewConsumerGroup()`.
//
//
// `ConsumerGroupHandler` is an implementation of `sarama.ConsumerGroupHandler`.
//
//
// `Topics` is a kafka topic to be subscribed.
type SaramaKafkaConsumserGroupAdapterConfig struct {
	ConsumerGroupClient  sarama.ConsumerGroup
	ConsumerGroupHandler sarama.ConsumerGroupHandler
	Topics               []string
}

type SaramaKafkaConsumserGroupAdapter struct {
	logger    *logrus.Logger
	closeChan chan struct{}
	config    *SaramaKafkaConsumserGroupAdapterConfig
}

type SaramaKafkaConsumserGroupAdapterProperty struct {
	Addresses                 []string
	GroupID                   string
	Topics                    []string
	ConsumerGroupHandler      sarama.ConsumerGroupHandler
	SaramaConfig              *sarama.Config
	CreateSaramaConsumerGroup CreateSaramaConsumerGroup
}

func NewSaramaKafkaConsumerGroupAdapterWithProperty(logger *logrus.Logger, property SaramaKafkaConsumserGroupAdapterProperty) (subscriber SubscriberCloser, err error) {
	if property.CreateSaramaConsumerGroup == nil {
		property.CreateSaramaConsumerGroup = DefaultCreateSaramaConsumerGroup
	}

	consumerGroupClient, err := property.CreateSaramaConsumerGroup(
		property.Addresses,
		property.GroupID,
		property.SaramaConfig,
	)
	if err != nil {
		return
	}

	config := &SaramaKafkaConsumserGroupAdapterConfig{
		ConsumerGroupClient:  consumerGroupClient,
		ConsumerGroupHandler: property.ConsumerGroupHandler,
		Topics:               property.Topics,
	}

	subscriber = NewSaramaKafkaConsumserGroupAdapter(logger, config)
	return
}

func NewSaramaKafkaConsumserGroupAdapter(logger *logrus.Logger, config *SaramaKafkaConsumserGroupAdapterConfig) SubscriberCloser {
	closeChan := make(chan struct{}, 1)
	return &SaramaKafkaConsumserGroupAdapter{
		logger,
		closeChan,
		config,
	}
}

func (skcga *SaramaKafkaConsumserGroupAdapter) Subscribe() {
	go func() {
	POLL:
		for {
			select {
			case <-skcga.closeChan:
				break POLL
			default:
				err := skcga.config.ConsumerGroupClient.Consume(context.Background(), skcga.config.Topics, skcga.config.ConsumerGroupHandler)
				if err != nil {
					skcga.logger.Errorf("[Sarama] %s", err.Error())
				}
			}
		}
	}()
}

// Close will stop the kafka consumer
func (skcga *SaramaKafkaConsumserGroupAdapter) Close() (err error) {
	defer close(skcga.closeChan)

	skcga.closeChan <- struct{}{}

	err = skcga.config.ConsumerGroupClient.Close()
	if err != nil {
		skcga.logger.Errorf("[Sarama] Consumer is closed with error. | %s", err.Error())
		return
	}

	skcga.logger.Info("[Sarama] Consumer is gracefully shut down.")
	return
}
